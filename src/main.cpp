#include "ncurses_display.h"
#include "system.h"
#include "linux_parser.h"
#include <iostream>

int main() {
  //float LinuxParser::MemoryUtilization();
  //std::cout << "china";
  System system;
  system.Cpu();

  /*  GET CPU NAME. IE intel 3570K
  std::string pretty = "PRETTY_NAME";
  std::string empty = "";
  LinuxParser::OperatingSystem(pretty, LinuxParser::kOSPath, ""); // kOSPath, LinuxParser::kProcDirectory + LinuxParser::kVersionFilename
   */
  /* GET KERNEL. IE: 5.8.0-63-generic
   LinuxParser::Kernel(LinuxParser::kProcDirectory, LinuxParser::kVersionFilename); // kProcDirectory + kVersionFilename
   */

  /* MEMORY UTILIZATION - .93
  // used= total – free – buff/cache // memory utilization
  //  Memory Utilization = 1.0 - (free_memory / (total_memory - buffers))
  //LinuxParser::Kernel(LinuxParser::kProcDirectory, LinuxParser::kMeminfoFilename); // good compared to OS filesystem
  // LinuxParser::OperatingSystem("MemTotal:", LinuxParser::kProcDirectory, LinuxParser::kMeminfoFilename);
  //LinuxParser::MemoryUtilization();
   */

  /* Total processes -> ie. 83592
  LinuxParser::TotalProcesses("processes", LinuxParser::kProcDirectory, LinuxParser::kStatFilename); // kProcDirectory + kStatFilename
  */

  /* Number of Process Running -> ie. 2
  LinuxParser::TotalProcesses("procs_running", LinuxParser::kProcDirectory, LinuxParser::kStatFilename);
 */

  /* Linux Uptime -> just the first number. ie. 365461.27
  LinuxParser::UpTime(LinuxParser::kProcDirectory, LinuxParser::kUptimeFilename);
  */

  Processor processor;
  processor.Utilization();
  // "cpu", LinuxParser::kProcDirectory, LinuxParser::kStatFilename
  //LinuxParser::AggCPUtilization();

   //NCursesDisplay::Display(system);
 }