#include "linux_parser.h"

#include <dirent.h>
#include <unistd.h>

#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <iostream>

using std::stof;
using std::string;
using std::to_string;
using std::vector;
//using std::cout;

// DONE: An example of how to read data from the filesystem
string LinuxParser::OperatingSystem(std::string s, const std::string filename_path, const std::string filename_path_) {
  string line;
  string key;
  string value;
  std::ifstream filestream(filename_path + filename_path_);
  if (filestream.is_open()) {
    while (std::getline(filestream, line)) {
      std::replace(line.begin(), line.end(), ' ', '_');
      std::replace(line.begin(), line.end(), '=', ' ');
      std::replace(line.begin(), line.end(), '"', ' ');
      std::istringstream linestream(line);
      while (linestream >> key >> value) {
        if (key == s) {
          std::replace(value.begin(), value.end(), '_', ' ');
          std::cout << "VALUE IS " << value;
          return value;
        }
      }
    }
  }
  std::cout << "VALUE OUTSIDE " << value;
  return value;
}

//std::string LinuxParser::OperatingSystem(std::string = "PRETTY_NAME");

// DONE: An example of how to read data from the filesystem
string LinuxParser::Kernel(std::string const filepath_name, std::string const filepath_name_) {
  string os, kernel, version;
  string line;
  std::ifstream stream(filepath_name + filepath_name_);
  if (stream.is_open()) {
    std::getline(stream, line);
    std::istringstream linestream(line);
    //linestream >> os >> version >> kernel;
    linestream >> os >> kernel;

  }
  std::cout << "KERNEL " << kernel;
  return kernel;
}

/*float LinuxParser::MemoryUtilization() {
  string line;
  std::ifstream stream(kProcDirectory + kMeminfoFilename);
 return 0.0;
}*/

// BONUS: Update this to use std::filesystem
vector<int> LinuxParser::Pids() {
  vector<int> pids;
  DIR* directory = opendir(kProcDirectory.c_str());
  struct dirent* file;
  while ((file = readdir(directory)) != nullptr) {
    // Is this a directory?
    if (file->d_type == DT_DIR) {
      // Is every character of the name a digit?
      string filename(file->d_name);
      if (std::all_of(filename.begin(), filename.end(), isdigit)) {
        int pid = stoi(filename);
        pids.push_back(pid);
      }
    }
  }
  closedir(directory);
  return pids;
}



// TODO: Read and return the system memory utilization
float LinuxParser::MemoryUtilization() {
  string line;
  string key;
  string value;
  float MemTotal, MemFree, MemAvailable, Buffers;
  float Utilization;
  std::ifstream filestream(kProcDirectory + kMeminfoFilename);
  if (filestream.is_open()) {
    while (std::getline(filestream, line)) {
      //std::cout << "LINE " << line;
      /*if (line == "MemTotal") {
        std::cout << "MemTotal: " << line;
      }*/
      std::istringstream linestream(line);
      while (linestream >> key >> value) {
        if (key == "MemTotal:") {
          //std::cout << "VALUE MemUtil: " << "\n" << value;
          // MemTotal = value;
          MemTotal = std::stof(value);
          std::cout << "VALUE MemUtil After " << "\n" << MemTotal;
        }
        if (key == "MemFree:") {
          MemFree = std::stof(value);
          std::cout << "VALUE MemFree After " << "\n" << MemFree;
        }
        if (key == "MemAvailable:") {
          MemAvailable = std::stof(value);
          std::cout << "VALUE MemAvailable After " << "\n" << MemAvailable;
        }
        if (key == "Buffers:") {
          Buffers = std::stof(value);
          std::cout << "VALUE Buffers After " << "\n" << Buffers;
        }
      }

    }

    // return 0.0;

  }
  Utilization = 1.0 - (MemFree / (MemTotal - Buffers));
  std::cout << "\n UTILIZATION " << Utilization;
  //Memory Utilization = 1.0 - (free_memory / (total_memory - buffers))

  return Utilization;
}

int LinuxParser::TotalProcesses(std::string procs, std::string filename_path, std::string filename_path_) {
  string line;
  string key;
  int value;
  // float MemTotal, MemFree, MemAvailable, Buffers;
  // float Utilization;
  std::ifstream filestream(filename_path + filename_path_);
  if (filestream.is_open()) {
    while (std::getline(filestream, line)) {
      std::istringstream linestream(line);
      while (linestream >> key >> value) {
        if (key == procs) {
          std::cout << "\n PROCESSES " << value;
          //value = std::stoi(value);
        }
      }
    }
  }
  return value;
}

long LinuxParser::UpTime(std::string filename_path, std::string filename_path_) {
  string line;
  long key;
  string value;
  // float MemTotal, MemFree, MemAvailable, Buffers;
  // float Utilization;
  std::ifstream filestream(filename_path + filename_path_);
  if (filestream.is_open()) {
    while (std::getline(filestream, line)) {
      std::istringstream linestream(line);
      while (linestream >> key >> value) {
        // std::cout << "KEY Uptime IS " << key << "\n" << "VALUE Uptime IS " << value;
        //std::string key_uptime = key + " " + " " + value;
        std::cout << "KEY UPTIME IS \n" << key;
        break;
        //std::cout << "value UPTIME IS \n" << value;
        //long key_ = std::stoll(key);
        //std::cout << "KEY UPTIME IS " << key_;
        /*if (key == procs) {
          std::cout << "\n PROCESSES " << value;
          //value = std::stoi(value);*/
        }
      }
    //std::cout << "KEY OUTSIDE \n" << key;
    }
  //std::cout << "KEY LAST \n" << key;
  return key;
  }

  int LinuxParser::RunningProcesses(std::string procs, std::string filename_path, std::string filename_path_) {
  string line;
  string key;
  int value;
  // float MemTotal, MemFree, MemAvailable, Buffers;
  // float Utilization;
  std::ifstream filestream(filename_path + filename_path_);
  if (filestream.is_open()) {
    while (std::getline(filestream, line)) {
      std::istringstream linestream(line);
      while (linestream >> key >> value) {
        if (key == procs) {
          std::cout << "\n KEY \n" << key;
          std::cout << "\n VALUE \n" << value;
          //value = std::stoi(value);
        }
      }
    }
  }
  return value;
}

/*float LinuxParser::AggCPUtilization(std::string procs, std::string filename_path, std::string filename_path_) {
  string line;
  string key;
  float user, nice, system, idle, iowait, irq, softirq, steal, guest, guest_nice;
  float value, value2, value3, value4, value5, value6, value7, value8, value9, value10;
  // float MemTotal, MemFree, MemAvailable, Buffers;
  // float Utilization;
  std::ifstream filestream(filename_path + filename_path_);
  if (filestream.is_open()) {
    while (std::getline(filestream, line)) {
      std::istringstream linestream(line);
      while (linestream >> key >> value >> value2 >> value3 >> value4 >> value5 >> value6 >> value7 >> value8 >> value9 >> value10) {
        if (key == procs) {
          std::cout << "\n CPU KEY \n" << key;
          std::cout << "\n CPU VALUE \n" << value;
          user = value;
          std::cout << "\n CPU USER \n" << user;
          std::cout << "\n CPU VALUE 2 \n" << value2;
          nice = value2;
          //nice = std::stof(value2);
          std::cout << "\n CPU VALUE 3 \n" << value3;
          system = value3;
          std::cout << "\n CPU VALUE 4 \n" << value4;
          idle = value4;
          std::cout << "\n CPU VALUE 5 \n" << value5;
          iowait = value5;
          std::cout << "\n CPU VALUE 6 \n" << value6;
          irq = value6;
          std::cout << "\n CPU VALUE 7 \n" << value7;
          softirq = value7;
          std::cout << "\n CPU VALUE 8 \n" << value8;
          steal = value8;
          std::cout << "\n CPU VALUE 9 \n" << value9;
          guest = value9;
          std::cout << "\n CPU VALUE 10 \n" << value10;
          guest_nice = value10;
          //value = std::stoi(value);
        }
      }
    }
  }
  // float previdle = previdle + previowait;
  return 0.0;
}*/



// TODO: Read and return the system uptime
//long LinuxParser::UpTime() { return 0; }

// TODO: Read and return the number of jiffies for the system
long LinuxParser::Jiffies() { return 0; }

// TODO: Read and return the number of active jiffies for a PID
// REMOVE: [[maybe_unused]] once you define the function
long LinuxParser::ActiveJiffies(int pid[[maybe_unused]]) { return 0; }

// TODO: Read and return the number of active jiffies for the system
long LinuxParser::ActiveJiffies() { return 0; }

// TODO: Read and return the number of idle jiffies for the system
long LinuxParser::IdleJiffies() { return 0; }

// TODO: Read and return CPU utilization
vector<string> LinuxParser::CpuUtilization() { return {}; }

// TODO: Read and return the total number of processes
// int LinuxParser::TotalProcesses() { return 0; }

// TODO: Read and return the number of running processes
// int LinuxParser::RunningProcesses() { return LinuxParser::TotalProcesses("procs_running", LinuxParser::kProcDirectory, LinuxParser::kStatFilename); }

// TODO: Read and return the command associated with a process
// REMOVE: [[maybe_unused]] once you define the function
string LinuxParser::Command(int pid[[maybe_unused]]) { return string(); }

// TODO: Read and return the memory used by a process
// REMOVE: [[maybe_unused]] once you define the function
string LinuxParser::Ram(int pid[[maybe_unused]]) { return string(); }

// TODO: Read and return the user ID associated with a process
// REMOVE: [[maybe_unused]] once you define the function
string LinuxParser::Uid(int pid[[maybe_unused]]) { return string(); }

// TODO: Read and return the user associated with a process
// REMOVE: [[maybe_unused]] once you define the function
string LinuxParser::User(int pid[[maybe_unused]]) { return string(); }

// TODO: Read and return the uptime of a process
// REMOVE: [[maybe_unused]] once you define the function
// long LinuxParser::UpTime(int pid[[maybe_unused]]) { return 0; }
